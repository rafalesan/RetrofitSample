package ni.com.cajina.retrofitsample;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by Rafael Alegria on 01/03/2018.
 */

public interface Api {

    String URL = "http://api.mathjs.org/";

    @GET("v4")
    Call<String> calcular(@Query("expr") String expr);


    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();
}
