package ni.com.cajina.retrofitsample;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private EditText etExp;
    private Button btnCalcular;
    private TextView tvResultado;
    private ProgressBar progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etExp = findViewById(R.id.et_exp);
        btnCalcular = findViewById(R.id.btn_calcular);
        tvResultado = findViewById(R.id.tv_resultado);
        progress = findViewById(R.id.progress);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realizarPeticion(etExp.getText().toString());
            }
        });

    }

    private void habilitarProgress(boolean habilitar) {
        progress.setVisibility(habilitar ? View.VISIBLE : View.INVISIBLE);
        btnCalcular.setEnabled(!habilitar);
    }

    private void realizarPeticion(String expr) {

        habilitarProgress(true);

        Api api = Api.retrofit.create(Api.class);
        Call<String> call = api.calcular(expr);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(@NonNull Call<String> call, @NonNull Response<String> response) {
                habilitarProgress(false);
                if(!response.isSuccessful()) {
                    toast("Ha ocurrido un error");
                    return;
                }
                tvResultado.setText(getString(R.string.resultado_ ,response.body()));
            }

            @Override
            public void onFailure(@NonNull Call<String> call, @NonNull Throwable t) {
                habilitarProgress(false);
                toast(t.getMessage());
            }
        });

    }

    private void toast(String msg) {
        Toast.makeText(MainActivity.this, msg, Toast.LENGTH_LONG).show();
    }

}
